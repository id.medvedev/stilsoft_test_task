from .base_page import BasePage
from .locators import CategoryPageLocators


class CategoryPage(BasePage):
    def collecting_links(self):
        product_page_links = self.browser.find_elements(*CategoryPageLocators.PRODUCT_PAGE_LINK)
        product_page_links_arr = []
        for product_page_link in product_page_links:
            product_page_links_arr.append(product_page_link.get_attribute("href"))
        return product_page_links_arr