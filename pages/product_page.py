import re
from .category_page import CategoryPage
from .locators import ProductPageLocators


class ProductPage(CategoryPage):
    def should_be_product_photo(self):
        try:
            assert self.is_element_present(*ProductPageLocators.PHOTO), "Product photo is not presented"
        except:
            return 0

    def product_service_life_with_7_years(self):
        try:
            service_life = re.sub('[\D]+', '', self.browser.find_element(*ProductPageLocators.ELSPEC).text)
            assert int(service_life) >= 7, "Service life less than 7 years"
        except:
            return 0