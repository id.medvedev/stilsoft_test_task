from selenium.webdriver.common.by import By

class CategoryPageLocators():
    PRODUCT_PAGE_LINK = (By.CSS_SELECTOR, '.product > a.product_anker:nth-child(2)')

class ProductPageLocators():
    PHOTO = (By.CSS_SELECTOR, ".imgCont")
    ELSPEC = (By.XPATH, "//div[contains(@class, 'r-tabs-panel')]/ul/li[contains(text(), 'Назначенный срок службы')]")