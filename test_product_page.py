from pages.category_page import CategoryPage
from pages.product_page import ProductPage


#category_link = "https://stilsoft.ru/products/kitsoz-synerget"
category_link = "https://stilsoft.ru/products/safe-city"


class TestElementsFromProductPage():
    def test_collecting_links_from_category_pages_and_checking_elements_in_product_pages(self, browser):
        category_page = CategoryPage(browser, category_link)
        category_page.open()
        product_page_links = category_page.collecting_links()
        bad_product = []
        for product_page_link in product_page_links:
            product_page = ProductPage(browser, product_page_link)
            product_page.open()
            check_photo = product_page.should_be_product_photo()
            if check_photo == 0:
                bad_product.append(product_page_link)
                continue
            check_espec = product_page.product_service_life_with_7_years()
            if check_espec == 0:
                bad_product.append(product_page_link)
                continue
        print(bad_product)
        f = open('results.txt', 'w+')
        for item in bad_product:
            f.write("%s\n" % item)
        f.close()
