# stilsoft_test_task



## About

This project created as test task for Stilsoft company.


## Installation

- [ ] clone repo;
- [ ] install python 3.x and modules fron requirements.txt;
- [ ] download and move chromedriver to path (version chromedriver = version GoogleChrome).


## Run

Command for running project:
```
pytest test_product_page.py
```
The result of project is the results.txt file.

## Enjoy